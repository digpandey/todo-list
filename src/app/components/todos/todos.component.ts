import { Component, OnInit } from '@angular/core';
import { todo } from '../../todo';

@Component({
  selector: 'app-todos',
  templateUrl: './todos.component.html',
  styleUrls: ['./todos.component.css']
})
export class TodosComponent implements OnInit {
  localitem: string;
  todos: todo[];
  constructor() {
    this.localitem = localStorage.getItem("todos");
    if (this.localitem == null) {

      this.todos = [];
    }
    else {
      this.todos = JSON.parse(this.localitem);
    }
  }

  ngOnInit(): void {
  }
  deleteTodo(todo: todo) {
    console.log(todo);
    const index = this.todos.indexOf(todo);
    this.todos.splice(index, 1)
    localStorage.setItem("todos", JSON.stringify(this.todos))
  }
  addtodo(todo: todo) {
    console.log(todo);
    this.todos.push(todo);
    localStorage.setItem("todos", JSON.stringify(this.todos))
  }
}