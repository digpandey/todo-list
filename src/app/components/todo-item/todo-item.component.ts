import { Input, Output } from '@angular/core';
import { Component, OnInit,EventEmitter } from '@angular/core';
import { todo } from 'src/app/todo';



@Component({
  selector: 'app-todo-item',
  templateUrl: './todo-item.component.html',
  styleUrls: ['./todo-item.component.css']
})
export class TodoItemComponent implements OnInit {
 @Input() todo:any;
 @Output() todoDelete: EventEmitter<todo> = new EventEmitter();
  constructor() { }

  ngOnInit(): void {
  }
onclick(todo:any){
  this.todoDelete.emit(todo) 
  // alert("This item is Deleted");
}
}
